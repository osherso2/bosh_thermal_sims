import math, numpy, matplotlib.pyplot as plt

"""
Taking cryogenic material property models from
from https://trc.nist.gov/cryogenics/materials/materialproperties.htm
"""

def al_specific_heat(T):

    # J/(kg K)
    a = 46.6467
    b = -314.292
    c = 866.662
    d = -1298.3
    e = 1162.27
    f = -637.795
    g = 210.351
    h = -38.3094
    i = 2.96344

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc

def ss_specific_heat(T):

    # J/(kg K)
    a = 22.0061
    b = -127.5528
    c = 303.647
    d = -381.0098
    e = 274.0328
    f = -112.9212
    g = 24.7593
    h = -2.239153
    i = 0.

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc

def nylon_specific_heat(T):

    # J/(kg K)
    a = -5.292
    b = 25.301
    c = -54.874
    d = 71.061
    e = -52.236
    f = 21.648
    g = -4.7317
    h = .42518
    i = 0.

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc

def al_thermal_cond(T):

    # W/(m K)
    a = .07918
    b = 1.0957
    c = -.07277
    d = .08084
    e = .02803
    f = -.09464
    g = .04179
    h = -.00571
    i = 0

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc

def cu_thermal_cond(T):

    """
    for RRR=100
    """
    # W/(m K)
    a = 2.2154
    b = -.47461
    c = -.88068
    d = .13871
    e = .29505
    f = -.02043
    g = -.04831
    h = .001281
    i = .003207

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc

def ss_thermal_cond(T):

    # W/(m K)
    a = -1.4087
    b = 1.3982
    c = .2543
    d = -.626
    e = .2334
    f = .4256
    g = -.4658
    h = .165
    i = -.0199 

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc

def nylon_thermal_cond(T):

    # W/(m K)
    a = -2.6135
    b = 2.3239
    c = -4.7586
    d = 7.1602
    e = -4.9155
    f = 1.6324
    g = -.2507
    h = .0131
    i = 0.

    lT = numpy.log10(T)
    lc = a + b*lT + c*lT**2. + d*lT**3. + e*lT**4. + f*lT**5. + g*lT**6. + h*lT**7. + i*lT**8.
    return 10.**lc


