import numpy, matplotlib.pyplot as plt
from sinks_and_links import sink, frozen_sink, link, powered_link, thermal_chain 
from cryo_mat import al_specific_heat, ss_thermal_cond 

####
#
#   A model for a 1W heater attached to stage1, a standoff, a stage2, a standoff, and a 70K tank. 
#
#   heater 1W -> [stage1] <-> standoffs1 <-> [stage2] <-> standoffs2 -> [tank (70 K)]
#   powered link -> sink  <->    link   <->   sink   <->   link   <->   frozen sink
#
####

# Let's make both stages 1 kg of Al
stage_mass = .1 # kg
C_stage = lambda T: stage_mass * al_specific_heat(T)
stage1 = sink(C = C_stage, T=77, name="stage1")
stage2 = sink(C = C_stage, T=75, name="stage2")

# Let's make the standoffs out of stainless steel
dim = .001 # m, this is cross-sectional area / length of the leg
G_leg = lambda T: dim*ss_thermal_cond(T)
standoffs1 = link(G_leg)

# conntect stage1 to stage2 through standoffs1
stage1.connect(stage2, standoffs1)

# Create a heater out of a powered links (needs a dummy sink)
heater = frozen_sink(T=5., name="heater")
stage1.connect(heater, powered_link(P=1)) # 1 W

# Create a heat sink / infinite heat capacity tank at 70 K (unchangable)
tank_T = 70 # K
tank = frozen_sink(T=tank_T, name="tank")
standoffs2 = link(G_leg)
stage2.connect(tank, standoffs2)

thm = thermal_chain(stage1)
thm.steady_state(numIters=25, verbose=True) # Solve for the steady state
thm.temps_to_pickle("example_savestate") # save steady state
thm.print_chain()
