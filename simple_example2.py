import numpy, matplotlib.pyplot as plt
from sinks_and_links import sink, frozen_sink, link, powered_link, thermal_chain 
from cryo_mat import al_specific_heat, ss_thermal_cond 

####
#
#   A model for a 1W heater attached to stage1, a standoff, a stage2, a standoff, and a 70K tank. 
#
#   heater 1W -> [stage1] <-> standoffs1 <-> [stage2] <-> standoffs2 -> [tank (70 K)]
#   powered link -> sink  <->    link   <->   sink   <->   link   <->   frozen sink
#
####

# Let's make both stages 1 kg of Al
stage_mass = .1 # kg
C_stage = lambda T: stage_mass * al_specific_heat(T)
stage1 = sink(C = C_stage, T=77, name="stage1")
stage2 = sink(C = C_stage, T=75, name="stage2")

# Let's make the standoffs out of stainless steel
dim = .001 # m, this is cross-sectional area / length of the leg
G_leg = lambda T: dim*ss_thermal_cond(T)
standoffs1 = link(G_leg)

# conntect stage1 to stage2 through standoffs1
stage1.connect(stage2, standoffs1)

# Create a heater out of a powered links (needs a dummy sink)
heater = frozen_sink(T=5., name="heater")
stage1.connect(heater, powered_link(P=1)) # 1 W

# Create a heat sink / infinite heat capacity tank at 70 K (unchangable)
tank_T = 70 # K
tank = frozen_sink(T=tank_T, name="tank")
standoffs2 = link(G_leg)
stage2.connect(tank, standoffs2)

thm = thermal_chain(stage1)
thm.temps_from_pickle("example_savestate") # load saved state 

####
#
#   Change power applied and watch the dynamic temperature(time)
#
####

heater.b_links[0].power = 1.25 # W

time_arr = numpy.arange(0, 60*60, 30) # 30 second steps... is this too aggressive?
stage1_temps = [stage1.T]
stage2_temps = [stage2.T]
for t, dt in zip(time_arr[1:], numpy.diff(time_arr)):

    if t == 30.*60.: heater.b_links[0].power = 1. # W
    if t % 360: thm.small_step(dt=dt, verbose=True) # v: verbose
    thm.small_step(dt=dt, verbose=False) # v: verbose
    stage1_temps.append(stage1.T)
    stage2_temps.append(stage2.T)

plt.plot(time_arr/60, stage1_temps, color='b', label="stage1")
plt.plot(time_arr/60, stage2_temps, color='g', label="stage2")

plt.title("Changing power applied")
plt.axvline(2*60, color='r')
plt.xlabel("time [minutes]")
plt.ylabel("stage temps [K]")
plt.legend()
plt.show()
