####
#
#   Thermal sim code I had kicking around. Not sure it's numerically sound, but it's cool.
#   -bosh 2020-06-02
#
####

from scipy.optimize import root, fmin
import numpy, pandas, sys

class sink:
    
	def __init__(self, C, T, name=None, T_bounds=(4, 300)):
                """
                Add an item with heat capacity. Can have as many thermal 'links' to other sinks as you want.

                Arguments
                =========
                C : function
                    Heat capacity as a function of temperature in J/K (use cryo_mat functions!)
                    You could, for example, put lambda T: .5(T/4.)**2. if you wanted to model a power law.
                T : float
                    Temperature in K
                name : string
                    A >>unique<< name to help with graph searching
                T_bounds : tuple
                    (lower limit, upper limit) on temperatures this sink's function can be used for.

                Returns                   
                =======
                Sink object
                """

		self.T = float(T)
		self.C = C
                self.T_bounds = T_bounds

                # Set of forward and backwards facing links. See connect function.
		self.a_links = []
		self.b_links = []
		
		self.name = name
                self.dynamic = True # as opposed to the frozen sink object.

                # Just computes energy stored in sink as a function of T
                # for a few temperatures and then interpolates and interpolates the inverse.
                try:
                    arr_T = numpy.arange(T_bounds[0], T_bounds[1], 1e-1)
                    sol = numpy.zeros(len(arr_T))
                    for i in range(len(arr_T)):
                        sol[i] = numpy.trapz(self.C(arr_T[:i+1]), x=arr_T[:i+1])
                    self.E_to_T = lambda x: numpy.interp(x, sol, arr_T) 
                    self.T_to_E = lambda x: numpy.interp(x, arr_T, sol) 
                except:
                    self.T_to_E = None
                    self.E_to_T = None

	def get_C(self, T=None):
                """
                Compute the heat capacity

                Argument
                ========
                T : float (optional)
                    Temperature in K, current object's temperature if left none.
                
                Returns
                =======
                heat capacity at T in J/K
                """

                if T is None: T = self.T
                assert(T > self.T_bounds[0] and T < self.T_bounds[1])
		return self.C(T)

	def E_dep(self, E, dT=1e-6):
                """
                Computes the change in temperature from an energy deposition.
                If the energy is small, just returns E/C.
                Else it solves for the integral bound such that integral(C, T=current temperature, T=???) = E
                Else it can use the inverse E to T relationship if you initialized this object with compute_TE=True

                Arguments
                =========
                E : float
                    Energy deposited in J
                dT : threshhold --- triggers integral fit if E/C > dT and then uses this dT as the integral discretization.

                Returns
                =======
                Sets this objects temperature to new temperature and returns new temperature
                """
		if E == 0: return self.T
                if not self.T_to_E is None and not self.T_to_E is None: 
                    assert(self.T > self.T_bounds[0] and self.T < self.T_bounds[1])
                    self.T = self.E_to_T(E + self.T_to_E(self.T))
                    return self.T 

	        nT = E/self.get_C()
                self.T += nT
		return self.T 

	def connect(self, sink_b, link):
                """
                Connects self to another sink through a link. This link will be appended to self.a_link array and to
                the other sink's b_link. Accordingly, self is this link's sink_a and the other is link.sink_b.

                The reason for this directionality is to avoid getting stuck in loops in the graph/network.

                Arguments
                =========
                sink_b : sink object
                    Another sink you want to connect to.
                link : link object
                    The link object connecting the two sinks.
                """

		link.sink_a = self
		link.sink_b = sink_b
	
                if link.name is None: link.name = self.name + "_" + sink_b.name

		self.a_links.append((link))
		sink_b.b_links.append((link))

	def string_sink(self, print_bool=True):
                """
                Generate a string describing current state of self.

                Arguments
                =========
                print_bool : bool (optional)
                    string can be sent through std.out as well as returned

                Returns
                =======
                string describing self.

                Example output
                ==============       
                stage  C(7.0e+1 K) = inf J/K 
                """

		string = ""
		string+= self.name
		string+= '\t'
		string+= 'C(T=%.1E K) = %.1E J/K' % (self.T, self.get_C())
		link_strings = []
		for link in self.a_links:
			s = link.string_link()
			s+= '\t'
			s+= 'connected to %s' % link.sink_b.name
			link_strings.append(s)

		if print_bool:
			print string
			for s in link_strings: print "\t", s
		return string, link_strings
	
	def copy(self, name=None):
                """
                Return an identical sink. This is useful to create a sink, then
                use it to make sink01, sink02, .... etcetera
                Note: This should not copy connections. 

                Arguments
                =========
                name : string.
                    New name to give copy. If None, new name is self.name+"_copy"
                    
                Returns
                =======
                sink 
                """

                if name is None: name = self.name + "_copy"
		return sink(self.C, self.T, name=name)

class frozen_sink(sink):

        def __init__(self, T, name=None):
                """
                A sink with infitite heat capacity; Keeps a constant temperature.
                This must have the identical function signatures as sink so it can work in place.
                """

                sink.__init__(self, C=lambda T: numpy.inf, T=T, name=name)
                self.dynamic = False 

	def E_dep(self, E, dT=1e-6):
                """
                Returns self.T, unchanged.

                Arguments
                =========
                E : float (IGNORED)
                    Energy in J.
                dT : float (IGNORED)
                    Threshhold temperature not in use.

                Returns
                =======
                T : float
                    self's temperature in K
                """
		return self.T

	def connect(self, sink_b, link):
                """
                Connects self to another sink through a link. This link will be appended to self.a_link array and to
                the other sink's b_link. Accordingly, self is this link's sink_a and the other is link.sink_b.

                The reason for this directionality is to avoid getting stuck in loops in the graph/network.

                Arguments
                =========
                sink_b : sink object
                    Another sink you want to connect to.
                link : link object
                    The link object connecting the two sinks.
                """

		link.sink_a = self
		link.sink_b = sink_b
		
		self.a_links.append((link))
		sink_b.b_links.append((link))

	def string_sink(self, print_bool=True):
                """
                Generate a string describing current state of self.

                Arguments
                =========
                print_bool : bool (optional)
                    string can be sent through std.out as well as returned

                Returns
                =======
                string describing self.

                Example output
                ==============
                stage  C(7.0e+1 K) = inf J/K 
                    connected to leg00
                """

		string = ""
		string+= self.name
		string+= '\t'
		string+= 'C(T=%.1E K) = %.1E J/K' % (self.T, self.get_C())
		link_strings = []
		for link in self.a_links:
			s = link.string_link()
			s+= '\t'
			s+= 'connected to %s' % link.sink_b.name
			link_strings.append(s)

		if print_bool:
			print string
			for s in link_strings: print "\t", s
		return string, link_strings
	
	def copy(self, name=None):
                """
                Return an identical sink. This is useful to create a sink, then
                use it to make sink01, sink02, .... etcetera
                Note: This should not copy connections. 

                Arguments
                =========
                name : string.
                    New name to give copy. If None, new name is self.name+"_copy"
                    
                Returns
                =======
                sink 
                """

                if name is None: name = self.name + "_copy"
		return frozen_sink(self.T, name=name)

class link:

	def __init__(self, G, sink_a=None, sink_b=None, name=None):
                """
                My thermal link object to compute power flowing across two temperatures.
        
                Arguments
                =========
                G : func
                    Thermal conductivity as a function of temperature in W/K (use cryo_mat functions!)
                    You could, for example, put lambda T: .5(T/4.)**2. if you wanted to model a power law.
                sink_a : sink 
                    One end of the connection
                sink_b : sink
                    Other end of the connection
                name : string
                    A >>unique<< name labeling this link

                Returns
                =======
                link 
                    object that can compute the power flowing from sink_a to sink_b 
                    (positive if sink_a is a higher temperature than sink_b)
                """
                
		self.G = G

		self.sink_a = sink_a
		self.sink_b = sink_b

		self.name = name
                self.powered = False

	def get_G(self, T=None):
                """
                Compute the thermal conductivity 

                Argument
                ========
                T : float (optional)
                    Temperature in K, current object's temperature if left none.
                
                Returns
                =======
                thermal conductivity at T in J/K
                """

                if T is None: T = (self.sink_a.T + self.sink_b.T)/2.
		return self.G(T)

	def P(self, dT=1e-6):
                """
                Compute power flowing across the link from sink_a to sink_b (positive if heat goes a -> b)

                Arguments
                =========
                dT : float
                    Threshhold in K before instead of using G*Delta_T, this computes the integral assuming a linear gradient.

                Returns
                =======
                P : float
                    Power in W.
                """

		if self.sink_b is None: return 0

		Ta = self.sink_a.T
		Tb = self.sink_b.T
		if Ta == Tb: return 0
                if abs(Tb - Ta) <= dT: return  self.G((Ta + Tb)/2.)*(Ta - Tb)
	        if Ta < Tb: dT = -abs(dT)
    	        arr_T = numpy.arange(Tb, Ta, dT)
	        P = numpy.trapz(self.G(arr_T), x=arr_T)
	        return P 

	def string_link(self, print_bool=True):
	        """
                Generate a string describing current state of self.

                Arguments
                =========
                print_bool : bool (optional)
                    string can be sent through std.out as well as returned

                Returns
                =======
                string describing self.

                Example output
                ==============
                stage1_stage2	  G(T=1.4E+02 K) = 1.1E-02 W/K	connected to stage2
                """

		T = (self.sink_a.T + self.sink_b.T)/2.
		string = ""
		string+= self.name
		string+= '\t'
		string+= '  G(T=%.1E K) = %.1E W/K' % (T, self.get_G())
                if print_bool: print string
		return string

	def copy(self, name=None):
                """
                Return an identical link. This is useful to create a link, then
                use it to make link01, link02, .... etcetera
                Note: This should not copy connections. 

                Arguments
                =========
                name : string.
                    New name to give copy. If None, new name is self.name+"_copy"

                Returns
                =======
                link 
                """

                if name is None: name = self.name + "_copy"
		return link(self.G, sink_a=None, sink_b=None, name=name)

class powered_link(link):
    
    	def __init__(self, P, sink_a=None, sink_b=None, name=None):
                """
                A dummy thermal link that provides constant power flow.
        
                Arguments
                =========
                G : func
                    Thermal conductivity as a function of temperature in W/K (use cryo_mat functions!)
                    You could, for example, put lambda T: .5(T/4.)**2. if you wanted to model a power law.
                sink_a : sink 
                    One end of the connection
                sink_b : sink
                    Other end of the connection
                name : string
                    A >>unique<< name labeling this link

                Returns
                =======
                link 
                    object that can compute the power flowing from sink_a to sink_b 
                    (positive if sink_a is a higher temperature than sink_b)
                """
                link.__init__(self, G=lambda T: numpy.nan, sink_a=sink_a, sink_b=sink_b, name=name)

		self.power = P 
                self.powered = True

	def P(self, dT=1e-6):
                """
                Compute power flowing across the link from sink_a to sink_b (positive if heat goes a -> b)

                Arguments
                =========
                dT : float (IGNORED)
                    Threshhold for nothing. 

                Returns
                =======
                P : float
                    Power in W.
                """

	        return self.power

        def copy(self, name=None):
                """
                Return an identical link. This is useful to create a link, then
                use it to make link01, link02, .... etcetera
                Note: This should not copy connections. 

                Arguments
                =========
                name : string.
                    New name to give copy. If None, new name is self.name+"_copy"

                Returns
                =======
                link 
                """

                if name is None: name = self.name + "_copy"
		return powered_link(self.P, sink_a=None, sink_b=None, name=name)

class thermal_chain:

	def __init__(self, base):
                """
                Thermal model to execute graph based methods to simulate small changes.

                Arguments
                =========
                base : sink
                    First node / sink object.
                """

		self.base = base

	def search_name(self, name, sink=None):
                """
                Find the sink with the matching name.

                Arguments
                =========
                name : string
                    name to search for.
                sink : None
                    leave this alone.
                
                Returns
                =======
                    sink with matching name.                                
                """
		if sink is None: sink=self.base
	
		if sink.name == name: return sink

		for link in sink.a_links:
			next_sink = link.sink_b
			res = self.search_name(name, sink=next_sink)
			if not res is None: return res

		return None 

        def set_state(self, state):
                """
                Given a dictionary of names and temperatures; Sets that state.

                Arguments
                =========
                state : dict
                    dictionary = zip(names, temps)
                    where names are strings and temps are floats in K
                    
                Returns
                =======
                Nothing
                """

                for name in state.keys():

                    sink = self.search_name(name)
                    assert(not sink is None)
                    sink.T = state[name] 

	def print_chain(self, sink=None, names=None):
                """
                Prints current state of self.

                Arguments
                =========
                sink : None
                    leave this argument alone.
                names : None
                    leave this argument alone.

                Returns
                =======
                string describing self.
                """


		if sink is None: 
			print "\nThermal Model\n"
			sink=self.base
		if names is None: names = []

		name = sink.name
		if name not in names: sink.string_sink(print_bool=True)
		names.append(name)

		for link in sink.a_links:
			next_sink = link.sink_b
			res = self.print_chain(sink=next_sink, names=names)
			if not res is None: return res

		return None 

        def get_network(self, sink=None, network=None):
                """
                Returns a dictionary of name associated with information.
                Used internally, leave alone.
                """

                if sink is None: sink=self.base
                if network is None: network = {}
		name = sink.name
		if name not in network.keys() and sink.dynamic:
                    network[name] = {"T":sink.T, "C":sink.get_C(), "P":0, "Gs":{name:0.}} 
                    for link in sink.a_links:
                        if link.powered: network[name]["P"] += link.P()
                        else:
                            if link.sink_a.dynamic:
                                if link.sink_a.name not in network[name]["Gs"].keys():
                                    network[name]["Gs"][link.sink_a.name] = -link.get_G()
                                else: network[name]["Gs"][link.sink_a.name] -= link.get_G()
                            if link.sink_b.dynamic:
                                if link.sink_b.name not in network[name]["Gs"].keys():
                                    network[name]["Gs"][link.sink_b.name] = link.get_G()
                                else: network[name]["Gs"][link.sink_b.name] += link.get_G()
                            else:
                                network[name]["P"] -= link.P()
                    for link in sink.b_links:
                        if link.powered: network[name]["P"] += link.P()
                        else:
                            if link.sink_a.dynamic:
                                if link.sink_a.name not in network[name]["Gs"].keys():
                                    network[name]["Gs"][link.sink_a.name] = link.get_G()
                                else: network[name]["Gs"][link.sink_a.name] += link.get_G()
                            if link.sink_b.dynamic:
                                if link.sink_b.name not in network[name]["Gs"].keys():
                                    network[name]["Gs"][link.sink_b.name] = -link.get_G()
                                else: network[name]["Gs"][link.sink_b.name] -= link.get_G()
                            else:
                                network[name]["P"] += link.P()
		for link in sink.a_links:
			next_sink = link.sink_b
			network = self.get_network(sink=next_sink, network=network)
			if not network is None: return network 

		return network 

        def small_step(self, dt=1e-3, verbose=True, network=None):
                """
                Takes a small step; Computes heat moving and accordingly updates all temperatures.
    
                Arguments
                =========
                dt : float
                    step size in seconds (default 1e-3)
                network : None
                    leave this alone
                verbose : bool
                    print details of calculation.

                Returns
                =======
                Nothing
                """

                if network is None: network = self.get_network()
                names = network.keys()
                N = len(names)
                index = dict(zip(names, range(N)))

                vec_T = numpy.zeros(N)
                vec_C = numpy.zeros(N)
                vec_P = numpy.zeros(N)
                mat_G = numpy.matrix(numpy.zeros((N,N)))
                for name in names:
                    i = index[name]
                    vec_T[i] = network[name]["T"]
                    vec_C[i] = network[name]["C"]
                    vec_P[i] = network[name]["P"]
                    Gs = network[name]["Gs"]
                    for partner in Gs.keys():
                        j = index[partner]
                        mat_G[i, j] = Gs[partner] 

                system = {}
                system["names"] = names
                system["vec_T"] = vec_T
                system["vec_C"] = vec_C
                system["vec_P"] = vec_P
                system["mat_G"] = mat_G

                vec_tP = numpy.array(mat_G.dot(vec_T))[0]
                vec_dE = dt*(vec_tP + vec_P)
                vec_dT = vec_dE / vec_C
                state = dict(zip(names, vec_T + vec_dT))
                self.set_state(state)

                if verbose:
                    print "C dT/dt = G.T + P"
                    for name in names:
                        i = index[name]
                        a = "%s (%.4e K): (%.1e J/K) dT/dt = %s"
                        b = ""
                        for partner in names:
                            j = index[partner]
                            if not mat_G[i,j] == 0:
                                p = (partner, mat_G[i,j], vec_T[j])
                                b += "\n\t + from %s: (%.03e W/K %.03e K)" % p
                        if b == "": b = "\n\t + external: %.3 W" % (vec_P[i])
                        else: b = b + "\n\t + external: %.3e W" % (vec_P[i])
                        print a % (name, vec_T[i], vec_C[i], b)
                        print "\t ---------------------------" 
                        print "\t   total: %.4e W" % (vec_dE[i]/dt)
                        change = "\t for %.2e s: %.06e J  T %.06e K -> %.06e K,   dT = %.06e K"
                        print change % (dt, vec_dE[i], vec_T[i], vec_T[i] + vec_dT[i], vec_dT[i])
                        print

        def steady_state(self, numIters=20, step_ratio=0.1, verbose=True):
                """
                Given a network, solve for the equilibrium state.
                Note this updates temperatures in each iteration, so you can 
                run 20 iterations, save the state in a pickle file, and the resume.

                Arguments
                =========
                numIters : int
                    number of iterations to run. Each iteration inverts a matrix 
                    of size N_sinks x N_sinks
                step_ratio : float
                    inverting a matrix solves for the equilibrium temperature if 
                    Gs and Cs were constant and if the network has no kernel. If
                    these things aren't true, it's best to reduce the size of the 
                    step by a ratio. Default is 0.1
                verbose : bool
                    print details of calculation

                Returns
                =======
                Nothing
                """

                for i in range(numIters):
                    if verbose: print("iteration %d" % i) 
                    network = self.get_network()
                    names = network.keys()
                    N = len(names)
                    index = dict(zip(names, range(N)))

                    vec_T = numpy.zeros(N)
                    vec_C = numpy.zeros(N)
                    vec_P = numpy.zeros(N)
                    mat_G = numpy.matrix(numpy.zeros((N,N)))
                    for name in names:
                        j = index[name]
                        vec_T[j] = network[name]["T"]
                        vec_C[j] = network[name]["C"]
                        vec_P[j] = network[name]["P"]
                        Gs = network[name]["Gs"]
                        for partner in Gs.keys():
                            k = index[partner]
                            mat_G[j, k] = Gs[partner] 
                    vec_nT = numpy.array(mat_G.I.dot(-vec_P))[0]
                    vec_dT = step_ratio * (vec_nT - vec_T)
                    r = (numpy.dot(vec_C, numpy.abs(vec_dT))/numpy.dot(vec_C, vec_T))
                    if verbose: print "%.1f%% change" % (100.*r)
                    self.set_state(dict(zip(names, vec_T + vec_dT)))

	def temps_to_pickle(self, savename=None, sink=None, res=None):
                """
                Save model's current state as a pickle file.
                """

		if sink is None: sink=self.base
		
		if res is None: res = {}
		res[sink.name] = sink.T

		for link in sink.a_links:
			next_sink = link.sink_b
			res.update(self.temps_to_pickle(sink=next_sink, res=res))

		if savename is None: return res
		cols = res.keys(); des = pandas.DataFrame(columns=cols)
		for c in cols: des[c] = [res[c]]
		des.to_pickle(savename)

	def temps_from_pickle(self, pickle, sink=None):
                """
                Load model state from a pickle file.
                """

		if sink is None: 
			sink=self.base
		
		res = pandas.read_pickle(pickle)
		sink.T = float(res[sink.name])

		for link in sink.a_links:
			next_sink = link.sink_b
			self.temps_from_pickle(pickle, sink=next_sink)

